var ComponentCode = /** @class */ (function () {
    function ComponentCode() {
    }
    ComponentCode.prototype.main = function () {
        throw new Error("Should be overridden");
    };
    return ComponentCode;
}());
var currentComponent;
function loadComponent(component) {
    currentComponent = component;
    try {
        document.getElementById("componentTitle").innerText = component.title;
        fetch(component.htmlPath)
            .then(function (response) {
            return response.text();
        })
            .then(function (body) {
            document.getElementById("component").innerHTML = body;
            component.code.main();
        });
    }
    catch (e) {
        console.error(e);
    }
}
//# sourceMappingURL=componentLoader.js.map