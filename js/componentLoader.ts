interface Component {
    title: string,
    id: string,
    htmlPath: string,
    code: ComponentCode
}

class ComponentCode {
    main() {
        throw new Error("Should be overridden")
    }
}

let currentComponent: Component;

function loadComponent(component: Component) {
    currentComponent = component;
    try {
        document.getElementById("componentTitle").innerText = component.title;
        fetch(component.htmlPath)
            .then(function(response: Response) {
                return response.text()
            })
            .then(function(body: string) {
                document.getElementById("component").innerHTML = body;
                component.code.main();
            })
    } catch (e) {
        console.error(e);
    }
}
