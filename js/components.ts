/*
User Component
 */
const userComponent = {
    title: "Gebruikers",
    id: "users",
    htmlPath: "components/userComponent/users.html",
    code: new UsersComponentCode()
};

const dynamicContentComponent = {
    title: "Info & Programma",
    id: "dynamic",
    htmlPath: "components/dynamicContentComponent/dynamicContent.html",
    code: new DynamicContentComponent()
};