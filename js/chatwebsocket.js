//https://stackoverflow.com/questions/1586330/access-get-directly-from-javascript
var parts = window.location.search.substr(1).split("&");
var $_GET = {};
for (var i = 0; i < parts.length; i++) {
  var temp = parts[i].split("=");
  $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
}

function initialiseWS(){
  var sessionId = $_GET["sessionId"];
  window.ws = new WebSocket("wss://julius.romereis-hl.nl");

// Connection opened
  ws.addEventListener('open', function (event) {
    var message = JSON.stringify(
      {"channel": "authentication", "sessionId": sessionId}
    );
    console.log("Sending message: " + message);
    ws.send(message);
  });

// Listen for messages
  ws.addEventListener('message', function (event) {
    console.log("Received message: ");
    var data = JSON.parse(event.data);
    console.log(data);
    if(data.channel && data.channel === "msg"){
      addMessage(data);
    }
  });
}
function getAndLoadMessageHistory(callback){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      try {
        var msgs = JSON.parse(this.responseText);
        for (var i = 0; i < msgs.length; i++) {
          addMessage(msgs[i]);
        }
        callback();
      } catch (e) {
        console.error("Exception: " + e);
      }
    }
  };
  xhttp.open("GET", "https://augustus.romereis-hl.nl/getmsgs.php?sessionId=" + $_GET["sessionId"], true);
  xhttp.send();
}
